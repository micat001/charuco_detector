"""
Object detector node

Author: Marc Micatka
"""
import os
import sys
from pathlib import Path

pkg_root = Path(__file__).parent.parent.resolve()
sys.path.insert(0, str(pkg_root))

from nodes import charuco_utils
import copy
import threading
import message_filters
import yaml
import rospy
from ros_numpy import msgify, numpify
from sensor_msgs.msg import Image, CameraInfo
from std_msgs.msg import Header
import cv2
import cv2.aruco as aruco
import numpy as np


class Detector:
    def __init__(self) -> None:
        yaml_file_path = os.path.join(
            pkg_root, rospy.get_param("~parameters", "config/parameters.yaml")
        )
        with open(yaml_file_path, "r") as file:
            model_config = yaml.safe_load(file)

        # Initiate Detector
        self.aruco_dict, self.charuco_board = charuco_utils.get_charuco_board(
            "default_11x8_45mm"
        )
        self.center_x, self.center_y = charuco_utils.get_board_center(
            self.charuco_board
        )

        image_topic = "~image"
        info_topic = "~info"
        if model_config is None:
            rospy.logerr("No Metadata Loaded!")
        try:
            image_topic = model_config["image_topic"]
            info_topic = model_config["info_topic"]
        except KeyError as ex:
            rospy.logerr(f"Key Error: {ex}")

        rospy.loginfo(f"Listening for images from {image_topic}")
        rospy.loginfo(f"Listening for info from {info_topic}")

        #  Approx sync subscribed to get both dept1h and camera info messages
        self.depth_image_sub = message_filters.Subscriber(image_topic, Image)
        self.camera_info_sub = message_filters.Subscriber(info_topic, CameraInfo)
        approx_sync = message_filters.ApproximateTimeSynchronizer(
            [self.depth_image_sub, self.camera_info_sub], queue_size=5, slop=0.1
        )
        approx_sync.registerCallback(self.synchronized_callback)

        # Aruco stuff:
        self.aruco_dict, self.charuco_board = charuco_utils.get_charuco_board(
            "default_11x8_45mm"
        )
        # Setup Publications
        self.annotated_image_pub = rospy.Publisher(
            "~annotated_image", Image, queue_size=10
        )
        self._camera_matrix = None
        self._distortion_matrix = None
        self._current_image = None
        self._current_header = None
        self._current_info = None
        self._detections = None
        self._annotated_image = None
        self._has_new_image = False
        self._rate = rospy.Rate(10)
        self._image_lock = threading.Lock()

    def synchronized_callback(self, img_msg: Image, img_info_msg: CameraInfo):
        """
        Sets the value of the depth image
        Stores the values of the intrinsic matrix

        Args:
            depth_msg (Image)
            depth_info_msg (CameraInfo)
        """
        with self._image_lock:
            self._current_image = numpify(img_msg)
            self._camera_matrix = np.reshape(img_info_msg.K, (3, 3))
            self._distortion_matrix = np.array(img_info_msg.D)

            self._current_header = img_msg.header
        self.run()

    def run(self):
        """
        Run the detector
        """
        with self._image_lock:
            img = copy.copy(self._current_image)

        # Perform object detection per defined class method:
        if len(img.shape) == 3:
            img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        # Detect markers and charuco corners
        (
            aruco_corners,
            aruco_ids,
            camera_rvec,
            camera_tvec,
        ) = self.detect_charuco_board(img)

        if camera_rvec is None:
            rospy.logwarn("Couldnt charuco board!")

        if camera_rvec is not None:
            rospy.logwarn(
                "Detected charuco board! rvec = {}, tvec = {}".format(
                    camera_rvec, camera_tvec
                )
            )
            rot, _ = cv2.Rodrigues(camera_rvec)
            board_center = camera_tvec + rot @ np.reshape(
                [self.center_x, self.center_y, 0.0], (3, 1)
            )
            annotated_img = self.drawDetectedCornersCharuco(
                self._current_image, valid_charuco_corners, valid_charuco_ids
            )
            annotated_img_msg = msgify(Image, annotated_img, encoding="rgb8")
            annotated_img_msg.header = self._current_header
            self.annotated_image_pub.publish(annotated_img_msg)

    def detect_charuco_board(self, img):
        """Find charuco corners in the image and use them to estimate board position"""
        aruco_corners, aruco_ids, rejected_img_points = aruco.detectMarkers(
            img, self.aruco_dict
        )
        _, charuco_corners, charuco_ids = aruco.interpolateCornersCharuco(
            aruco_corners, aruco_ids, img, self.charuco_board
        )

        res = cv2.aruco.estimatePoseCharucoBoard(
            charuco_corners,
            charuco_ids,
            self.charuco_board,
            self._camera_matrix,
            self._distortion_matrix,
            None,
            None,
            False,
        )
        success, camera_rvec, camera_tvec = res

        if not success:
            camera_rvec, camera_tvec = None, None

        return (
            aruco_corners,
            aruco_ids,
            camera_rvec,
            camera_tvec,
        )

    def drawDetectedCornersCharuco(self, img, corners, ids):
        """
        Draw rectangles and IDs to the corners, wrapper

        Parameters
        ----------
        img : numpy.array()
            Two dimensional image matrix. Image can be grayscale image or RGB image
            including 3 layers. Allowed shapes are (x, y, 1) or (x, y, 3).
        corners : numpy.array()
            Checkerboard corners.
        ids : numpy.array()
            Corners' IDs.
        """
        drawn_image = img.copy()
        if ids.size > 0:
            id_color = (255, 255, 0)
            corners = corners.reshape((corners.shape[0], 1, corners.shape[1]))
            ids = ids.reshape((ids.size, 1))
            cv2.aruco.drawDetectedCornersCharuco(drawn_image, corners, ids, id_color)
        return drawn_image

    def get_annotations(self) -> Image:
        """
        Get the annotated image

        Returns:
            Image: annotated image message
        """
        return self._annotated_image

    def get_detections(self):
        """
        Get the detection message

        Returns:
            Detections: Detections message
        """
        return self._detections


if __name__ == "__main__":
    try:
        rospy.init_node("charuco_detector")
        detector = Detector()
        rospy.loginfo("Starting Charuco Detector.")
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
