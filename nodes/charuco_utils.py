# Author: Laura Lindzey
# Copyright 2020-2022 University of Washington Applied Physics Laboratory
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
# THE POSSIBILITY OF SUCH DAMAGE.

import cv2
import cv2.aruco
import numpy as np

# I'd rather have these be stored as configuration than as code, but putting
# them here makes it easier to import this file from whatever package needs
# it, and I don't want to have to mess with installing a .txt file somewhere.

# Given that the IDs are global variables, the dictionary name should also be one.
# If we run out of tags, we can bump this up to DICT_4X4_1000 without breaking anything,
# since the first 250 tags in _1000 match _250.
_aruco_dict_id = cv2.aruco.DICT_4X4_250

# This list should only be appended to, and must be updated when new tags are ordered.
_fiducial_ids = []
# List of IDs that have been used on tank fiducials that have been manufactured.
_fiducial_ids.extend(
    [5, 6, 7, 8, 9, 11, 12, 13, 14, 15, 19, 21, 23, 24, 25, 26, 28, 29, 30, 31, 32]
)
# IDs dedicated for the OpenROV project. If we run out, these can probably
# be added into the plinth set (only need to be distinct from the tank markers)
_fiducial_ids.extend([0, 3, 4, 33, 34, 35])
# IDs of the 24 4x4" floor tags that we ordered.
_fiducial_ids.extend([37, 38, 42, 43, 44, 46, 47, 48, 140, 143, 144, 148, 149,
                      150, 151, 152, 153, 154, 156, 157, 160, 161, 162, 163])  # fmt:skip

# White list of tags that may be used on a plinth / charuco target.
_plinth_ids = [51, 56, 58, 60, 62, 63, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74,
               75, 76, 78, 79, 80, 81, 82, 83, 84, 86, 87, 88, 91, 92, 94, 95,
               96, 97, 99, 100, 101, 103, 104, 105, 106, 107, 108, 109, 112,
               114, 115, 117, 119, 122, 124, 125, 126, 127, 128, 129, 132, 133,
               134, 135, 137, 139]  # fmt: skip


def is_good_marker(aruco_dict, idx):
    # type: (cv2.aruco_Dictionary, int) -> bool
    """
    The requirement for being a "good" marker is that we can easily pick
    out the very center of the marker, which means that in the central square
    of 4 pixels, at least one of the diagonals has the same color, while one
    of the off-diagonal squares is different.
    """
    # NB: This is assuming that we have a single border bit; I'm not sure how else to get
    #     the minimal representation of the marker
    # NB: I _wanted_ to find the canonical representation of the marker, but couldn't figure out
    #     how to get that, so we're drawing it into as small a matrix as possible, and using the
    #     central pixels to determine "goodness".
    #    (I can't figure out how to get getBitsFromByteList to work)
    ms = aruco_dict.markerSize
    try:
        matrix = aruco_dict.drawMarker(idx, ms + 2)
    except Exception as ex:
        print("Could not draw marker assuming single border bit.")
        raise (ex)

    # If it doesn't have an even number of pixels, then there won't be a clear corner
    # in the exact center of the marker
    if aruco_dict.markerSize % 2 != 0:
        print("WARNING: NO marker with an odd number of pixels can be considered good")
        return False

    rr = int(aruco_dict.markerSize / 2)
    ul = matrix[rr][rr]
    ur = matrix[rr][rr + 1]
    ll = matrix[rr + 1][rr]
    lr = matrix[rr + 1][rr + 1]
    if ll == ur and ((ll != ul) or (ll != lr)):
        return True
    elif ul == lr and ((ul != ll) or (ul != ur)):
        return True
    else:
        return False


# Make sure that our whitelists are valid
# TODO: Consider also checking is_good_marker? However, that started seeming
#    like a lot to put in computation that happens at every import.

tmp_dict = cv2.aruco.getPredefinedDictionary(_aruco_dict_id)
for marker_id in _fiducial_ids:
    assert marker_id not in _plinth_ids
    assert is_good_marker(tmp_dict, marker_id)
for marker_id in _plinth_ids:
    assert marker_id not in _fiducial_ids
    assert is_good_marker(tmp_dict, marker_id)


def get_board_params(name):
    # type: String -> Tuple[int, int, float]
    """
    Given the name of a board/plinth, return its rows/cols and square width.
    """
    if name in ["default_11x8_45mm", "charuco_20210722_8x11_45mm"]:
        nrows = 8
        ncols = 11
        checker_size = 0.045
    elif name in ["default_11x8_30mm", "default_8x11_30mm"]:
        nrows = 8
        ncols = 11
        checker_size = 0.030
    elif name in ["default_8x11_15mm", "default_11x8_15mm"]:
        nrows = 8
        ncols = 11
        checker_size = 0.015
    elif name == "plinth_20201023_24x18":
        ncols = 12
        nrows = 9
        checker_size = 0.045  # meters
    elif name in ["plinth_20201026_22x22", "plinth_20210722_11x11_45mm"]:
        ncols = 11
        nrows = 11
        checker_size = 0.045  # meters
    else:
        raise Exception("Unsupported name in get_board_params: {}".format(name))
    return nrows, ncols, checker_size


def get_charuco_board(name):
    # type: string -> Tuple[cv2.aruco_Dictionary, cv2.aruco_CharucoBoard]
    """
    Given the name of a board/plinth, return the dictionary used and the board.

    The dictionary isn't used by most callers, but is handy to have for plotting
    the individual markers for debugging.
    """
    aruco_dictionary, charuco_board = None, None
    nrows, ncols, checker_size = get_board_params(name)

    if name in [
        "default_11x8_45mm",
        "default_8x11_45mm",
        "default_11x8_30mm",
        "default_8x11_30mm",
        "default_11x8_15mm",
        "default_8x11_15mm",
    ]:
        aruco_size = 0.75 * checker_size
        aruco_dictionary = cv2.aruco.getPredefinedDictionary(_aruco_dict_id)
        charuco_board = cv2.aruco.CharucoBoard_create(
            ncols, nrows, checker_size, aruco_size, aruco_dictionary
        )

    elif name in ["plinth_20201023_24x18", "plinth_20201026_22x22"]:
        # V1 of the plinth for collection reconstruction datasets
        # Whitelist of markers that we like:
        # * Excludes simple white rectangle
        # * Requires easily-identifiable center
        aruco_dictionary = cv2.aruco.getPredefinedDictionary(_aruco_dict_id)
        good_markers = [
            0,
            3,
            4,
            5,
            6,
            7,
            8,
            9,
            11,
            12,
            13,
            14,
            15,
            19,
            21,
            23,
            24,
            25,
            26,
            28,
            29,
            30,
            31,
            32,
            33,
            34,
            35,
            38,
            42,
            43,
            44,
            46,
            47,
            48,
            51,
            56,
            58,
            60,
            62,
            63,
            65,
            66,
            67,
            68,
            69,
            70,
            72,
            73,
            74,
            75,
            76,
            78,
            79,
            80,
            81,
            82,
            83,
            84,
            86,
            87,
            88,
            91,
            92,
            94,
            95,
            96,
            97,
            99,
        ]
        aruco_dictionary.bytesList = aruco_dictionary.bytesList[good_markers]

        aruco_size = 0.75 * checker_size
        charuco_board = cv2.aruco.CharucoBoard_create(
            ncols, nrows, checker_size, aruco_size, aruco_dictionary
        )

    elif name in [
        "plinth_20210722_11x11_45mm",
        "charuco_20210722_8x11_45mm",
        "plinth_20210723_11x14_45mm",
        "plinth_20210723_13x17_45mm",
    ]:
        # Handle the boards that use the new-style whitelist of tags
        aruco_dictionary = cv2.aruco.getPredefinedDictionary(_aruco_dict_id)
        # TODO: This hard-coded file location makes this function brittle.
        aruco_dictionary.bytesList = aruco_dictionary.bytesList[_plinth_ids]
        aruco_size = 0.75 * checker_size
        charuco_board = cv2.aruco.CharucoBoard_create(
            ncols, nrows, checker_size, aruco_size, aruco_dictionary
        )
    else:
        raise Exception("Unrecognized board name: {}".format(name))
    return aruco_dictionary, charuco_board


def get_board_center(board):
    """
    Return coordinates (in meters) of the center of the board.
    """
    ncols, nrows = board.getChessboardSize()
    ss = board.getSquareLength()
    return 0.5 * ss * ncols, 0.5 * ss * nrows


def detect_charuco_corners(aruco_dict, charuco_board, camera_data):
    """Locate charuco corners in the image"""
    # First, find locations of aruco markers in the image
    aruco_corners, aruco_ids, rejected_points = cv2.aruco.detectMarkers(
        camera_data, aruco_dict
    )

    if len(aruco_corners) == 0:
        return (aruco_corners, aruco_ids, rejected_points, None, None)

    aruco_ids, aruco_corners = deduplicate_aruco(
        charuco_board, aruco_ids, aruco_corners
    )

    # Using the aruco tags, find charuco corners
    retval, charuco_corners, charuco_ids = cv2.aruco.interpolateCornersCharuco(
        aruco_corners, aruco_ids, camera_data, charuco_board
    )
    if charuco_corners is None:
        return (aruco_corners, aruco_ids, rejected_points, None, None)

    # TODO: Subpixel refinement of corner locations?

    # Filter the detected corners to throw out bad corners due to
    # duplicate tags
    valid_charuco = [
        (idx, corner)
        for idx, corner in zip(charuco_ids, charuco_corners)
        if is_corner_valid(charuco_board, idx[0], aruco_ids, aruco_corners)
    ]

    if len(valid_charuco) == 0:
        return (aruco_corners, aruco_ids, rejected_points, None, None)

    # Finally, using charuco corners, determine the pose of the board
    valid_charuco_ids, valid_charuco_corners = map(np.array, zip(*valid_charuco))

    return (
        aruco_corners,
        aruco_ids,
        rejected_points,
        valid_charuco_corners,
        valid_charuco_ids,
    )


def detect_charuco_board(aruco_dict, charuco_board, camera_data, camera_info):
    """Find charuco corners in the image and use them to estimate board position"""
    cc = detect_charuco_corners(aruco_dict, charuco_board, camera_data)
    aruco_corners, aruco_ids, _, valid_charuco_corners, valid_charuco_ids = cc

    camera_matrix = np.reshape(camera_info.K, (3, 3))
    res = cv2.aruco.estimatePoseCharucoBoard(
        valid_charuco_corners,
        valid_charuco_ids,
        charuco_board,
        camera_matrix,
        camera_info.D,
        None,
        None,
        False,
    )
    success, camera_rvec, camera_tvec = res

    if not success:
        camera_rvec, camera_tvec = None, None

    return (
        aruco_corners,
        aruco_ids,
        valid_charuco_corners,
        valid_charuco_ids,
        camera_rvec,
        camera_tvec,
    )


def _get_image_dist(aruco_corners, idx1, vertex1, idx2, vertex2):
    """
    Get distance between the two specified marker/vertex pairs.

    This takes the index into the aruco_corners array, rather than the aruco_id
    because we care about disambiguating between duplicate detections.
    """
    x1 = aruco_corners[idx1][0][vertex1][0]
    x2 = aruco_corners[idx2][0][vertex2][0]
    dx = x2 - x1
    y1 = aruco_corners[idx1][0][vertex1][1]
    y2 = aruco_corners[idx2][0][vertex2][1]
    dy = y2 - y1
    return np.sqrt(dx * dx + dy * dy)


def deduplicate_aruco(charuco_board, aruco_ids, aruco_corners):
    """
    Filter out duplicate aruco tags based on which one is closest to the
    other detected tags that should be its neighbors in the board.

    If none of their caddy-corner neighbors are detected, then duplicate
    tags will be allowed to remain in the dataset, since this filtering
    method won't work (and they won't have any effect on the subsequent
    charuco board localization).
    """
    ccs = [
        CharucoCorner(cid, charuco_board)
        for cid in range(len(charuco_board.nearestMarkerIdx))
    ]

    bad_indices = set()
    for aruco_id in np.unique(aruco_ids):
        # If this ID isn't a duplicate, can skip it.
        indices = np.where(aruco_ids == aruco_id)[0]
        if len(indices) == 1:
            continue

        relevant_corners = [cc for cc in ccs if cc.contains_aruco_id(aruco_id)]

        if len(relevant_corners) == 0:
            print(
                "Aruco ID {} appears multiple times in the image, "
                "but is not part of any charuco corners. "
                "Cannot determine which detection(s) to discard".format(aruco_id)
            )
            continue

        detected_corners = [
            cc
            for cc in relevant_corners
            if cc.get_other_vertex(aruco_id).id in aruco_ids
        ]

        if len(detected_corners) == 0:
            print(
                "Aruco ID {} appears multiple times in the image, "
                "but no caddy-corner tags were detected. "
                "Cannot determine which detection(s) to discard.".format(aruco_id)
            )
            continue

        # Determine which of the indices corresponding to aruco_id is closest
        # to the rest of the board
        index_dists = {tag_idx: 0 for tag_idx in indices}
        for this_idx in index_dists.keys():
            for cc in detected_corners:
                # Unfortunately, it's *also* possible that the other tag in
                # the corner is a duplicate. In that case, we just want to
                # use the closest of all detected tags with the same ID.
                # I think that'll be robust to anything other than extreme
                # size differentials or multiple identical boards.
                this_vertex = cc.get_vertex(aruco_id)
                other_vertex = cc.get_other_vertex(aruco_id)
                other_idxs = np.where(aruco_ids == other_vertex.id)[0]

                corner_dist = np.inf
                for other_idx in other_idxs:
                    new_dist = _get_image_dist(
                        aruco_corners,
                        this_idx,
                        this_vertex.vertex,
                        other_idx,
                        other_vertex.vertex,
                    )
                    if new_dist < corner_dist:
                        corner_dist = new_dist
                index_dists[this_idx] += corner_dist

        min_dist = min(index_dists.values())
        for idx, idx_dist in index_dists.items():
            if idx_dist > min_dist:
                bad_indices.add(idx)

    good_indices = np.array(
        [ii for ii in range(len(aruco_ids)) if ii not in bad_indices]
    )

    good_aruco_corners = [
        cc for ii, cc in enumerate(aruco_corners) if ii not in bad_indices
    ]

    return aruco_ids[good_indices], np.array(good_aruco_corners)


def is_corner_valid(board, corner_id, marker_ids, marker_corners):
    """
    * board -- cv2.aruco_CharucoBoard
    * corner_id -- index of the corner that we want to check
    * marker_ids -- 1D array of lists of detected marker IDs.
        e.g. array([[42], [41], [36], ....])
    * marker_corners -- array of corners corresponding to each
        detected marker, in pixel coordinates.
        Order is top-left, top-right, lower-right, lower-left
        e.g. marker_corners[idx] = array([[[1720., 1427.], [1860., 1475.],
                                           [1804., 1603.], [1666., 1556.]]])
    """
    marker1_corner = board.nearestMarkerCorners[corner_id][0][0]
    marker2_corner = board.nearestMarkerCorners[corner_id][1][0]

    marker1_idx = board.nearestMarkerIdx[corner_id][0][0]
    marker2_idx = board.nearestMarkerIdx[corner_id][1][0]

    # Check that marker1 is closest to marker2, and vice-versa
    idx2, corner2 = _find_closest_corner(
        board, corner_id, marker1_idx, marker1_corner, marker_ids, marker_corners
    )
    if idx2 != marker2_idx or corner2 != marker2_corner:
        return False
    idx1, corner1 = _find_closest_corner(
        board, corner_id, marker2_idx, marker2_corner, marker_ids, marker_corners
    )
    if idx1 != marker1_idx or corner1 != marker1_corner:
        return False

    return True


def _find_closest_corner(
    board, corner_id, marker1_id, marker1_corner, marker_ids, marker_corners
):
    """
    Find the marker and its corner that is closest to the input marker.

    NB: This assumes that the input list of markers is unique;
        duplicate markers need to be filtered out before calling this.

    * board -- cv2.aruco_CharucoBoard
    * corner_id -- index of the corner that we want to check
    * marker1_id -- ID of one of the two markers in the corner
    * marker1_corner -- index into the corners array (indicates which corner we're searching for)
    * marker_ids -- all detected marker IDs in the iamge
    * marker_corners -- corresponding detected corners
    """
    # Find the pixel location of the input marker's specified corner
    xx, yy = None, None
    for idx, coords in zip(marker_ids, marker_corners):
        if marker1_id == idx:
            xx, yy = coords[0][marker1_corner]
    if xx is None:
        print("could not find input corner in board!")
        return None, None

    min_dist_squared = None
    min_dist_marker = None
    min_dist_corner = None
    for idx, coords in zip(marker_ids, marker_corners):
        if marker1_id == idx[0]:
            continue
        for corner, corner_coords in enumerate(coords[0]):
            dx = corner_coords[0] - xx
            dy = corner_coords[1] - yy
            dd = dx * dx + dy * dy
            if min_dist_squared is None or dd < min_dist_squared:
                min_dist_squared = dd
                min_dist_marker = idx[0]
                min_dist_corner = corner

    if min_dist_squared is None:
        print("Could not find closest corner")
        None, None
    return min_dist_marker, min_dist_corner


#############################
# Utility classes for working with aruco/charuco
class ArucoVertex(object):
    UL = 0  # Vertex index for upper-right corner
    UR = 1
    LR = 2
    LL = 3  # Vertex index for lower-left corner

    def __init__(self, aruco_id, vertex_id):
        self.id = aruco_id
        if vertex_id not in [self.UL, self.UR, self.LR, self.LL]:
            raise Exception("Unrecognized vertex id: {}".format(vertex_id))
        self.vertex = vertex_id

    def __repr__(self):
        return "{}({})".format(self.id, self.vertex)


class CharucoCorner(object):
    def __init__(self, corner_id, charuco_board):
        self.id = corner_id

        # This awfulness is the whole reason to define a class.
        self.vertex1 = ArucoVertex(
            charuco_board.nearestMarkerIdx[corner_id][0][0],
            charuco_board.nearestMarkerCorners[corner_id][0][0],
        )
        self.vertex2 = ArucoVertex(
            charuco_board.nearestMarkerIdx[corner_id][1][0],
            charuco_board.nearestMarkerCorners[corner_id][1][0],
        )

    def __repr__(self):
        return "Corner {}: vertices {} and {}".format(
            self.id, self.vertex1, self.vertex2
        )

    def contains_aruco_id(self, aruco_id):
        return aruco_id == self.vertex1.id or aruco_id == self.vertex2.id

    def get_vertex(self, aruco_id):
        if aruco_id == self.vertex1.id:
            return self.vertex1
        elif aruco_id == self.vertex2.id:
            return self.vertex2
        else:
            raise Exception(
                "Requested invalid aruco id {} from corner id {}".format(
                    aruco_id, self.id
                )
            )

    def get_other_vertex(self, aruco_id):
        if aruco_id == self.vertex1.id:
            return self.vertex2
        elif aruco_id == self.vertex2.id:
            return self.vertex1
        else:
            raise Exception(
                "Requested other vertex, but aruco id {} not in corner id {}".format(
                    aruco_id, self.id
                )
            )
